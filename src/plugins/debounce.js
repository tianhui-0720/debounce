/**
 * 
 * @param {Function} fn 回调函数
 * @param {Number} delay 延迟时间 默认200
 * @returns 
 */
export function Debounce(fn, delay) {
  // 记录上一次的延时器
  let timer = null;
  const time = delay || 200;
  return function() {
    const args = arguments;
    const that = this;
    // 清除上一次延时器
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(that, args)
    }, time);
  }
}
